import requests
import xml.etree.ElementTree as ET
from typing import Dict
from typing import List
import sys
import pickle
import pandas as pd
import time
from bs4 import BeautifulSoup
import json


def get_game_reviews_ids(game_id: str) -> Dict:
    """This method gets the list of reviews of a given game_id.

    Args:
        game_id: id of the boardgame you want to collect the review ids.

    Returns:
        Dictionary with game id and list of reviews"""

    # get forums given game_id
    forums = requests.get(
        f"https://www.boardgamegeek.com/xmlapi2/forumlist?id={game_id}&type=thing"
    )
    root_forums = ET.fromstring(forums.text)
    reviews_forum_id = [
        child.attrib["id"]
        for child in root_forums
        if (child.attrib["title"] == "Reviews")
    ][0]

    # get review forum given reviews_forum_id
    reviews = requests.get(
        f"https://www.boardgamegeek.com/xmlapi2/forum?id={reviews_forum_id}"
    )
    root_reviews = ET.fromstring(reviews.text)
    reviews_ids = [
        review.get("id") for review in root_reviews.findall("./threads/thread")
    ]
    game_review_ids = {"game_id": game_id, "reviews_ids": reviews_ids}

    return game_review_ids


# get reviews from list of game ids
def get_games_reviews_ids(game_ids: List):
    """This method gets the list of reviews ids for each game id from the list game_ids.

    Args:
        game_ids: list of boradgames you want to collect the review ids from.

    Returns:
        list of dictionaries containing game id and list of reviews for each game id given in game_ids"""

    games_with_review_ids = []
    for game_id in game_ids:
        try:
            game_review_ids = get_game_reviews_ids(str(game_id))
            games_with_review_ids.append(game_review_ids)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            print(game_id)
    return games_with_review_ids


def get_review_info(id_review: str, game_id: str):
    """This method gets information about one review (id_review).

    Args:
        id_review: id of the review you want to colect the information from.
        game_id: id of the boardgame the review is from.

    Returns:
        Dictionary with 'game_id', 'id_review', 'article_id', 'username', 'postdate', 'subject', and 'content'"""

    tryAgain = True
    while tryAgain:
        review = requests.get(
            f"https://www.boardgamegeek.com/xmlapi2/thread?id={id_review}&count=1"
        )
        try:
            root = ET.fromstring(review.text)
        except ET.ParseError as e:
            print(e)
        try:
            review = root.findall("./articles/article")[0]
            review_data = {
                "game_id": game_id,
                "id_review": id_review,
                "article_id": review.get("id"),
                "username": review.get("username"),
                "postdate": review.get("postdate"),
                "subject": review.find("subject").text,
                "content": review.find("body").text,
            }
            tryAgain = False
        except TimeoutError as e:
            time.sleep(60)
        except ConnectionError as e:
            time.sleep(60)
        except:
            if "<message>Rate limit exceeded.</message>" in review.text:
                time.sleep(10)
            else:
                tryAgain = False
                review_data = {
                    "game_id": game_id,
                    "id_review": id_review,
                    "article_id": "",
                    "username": "",
                    "postdate": "",
                    "subject": "",
                    "content": "",
                }
                print("Unexpected error:", sys.exc_info())

    return review_data


def collect_reviews_info(games_review_id: List, filename: str = "reviews.csv"):
    """This method gets information a list of reviews (games_review_id) and saves it to filename.

    Args:
        games_review_id: list of dictionaries containing game id and list of reviews for each game id given in
        game_ids. Output of the function get_games_reviews_ids()
        filename: name of the file you want the data to be saved to.

    Returns:
        Dictionary with 'game_id', 'id_review', 'article_id', 'username', 'postdate', 'subject', and 'content'"""

    print("collect_reviews_info() - This function takes a long time to run")
    try:
        reviews_df = pd.read_csv(filename)
        print(
            f"The file {filename} already exist, we will add to it any review that hasn't been added yet"
        )
    except:
        reviews_df = pd.DataFrame()

    try:
        for game in games_review_id:
            for review_id in game["reviews_ids"]:
                if (not reviews_df.empty) and (
                    int(review_id) in reviews_df.id_review.values
                ):
                    continue
                value = get_review_info(review_id, game["game_id"])
                reviews_df = reviews_df.append(value, ignore_index=True)
    except Exception as e:
        print(f"Outer exception: {e}")
        print(f"Current review_id: {review_id}")

    reviews_df.to_csv(filename, index=False)


def get_reviews(game_ids: List, filename: str):
    games_with_review_ids = get_games_reviews_ids(game_ids)
    collect_reviews_info(games_with_review_ids, filename)


def get_amazon_link(index: str, id: str, amazon_file):
    """Get amazon URL and save it to the open file.

    Args:
        index: index of the dataframe.
        id: game id
        amazon_file: open file
    """

    r = requests.get(
        f"https://api.geekdo.com/api/amazon/itemurls?locale=us&objectid={id}&objecttype=thing"
    )
    if r.text != "[]":
        amazon_widget = json.loads(r.text)["us"]
        r_amazon = requests.get("http:" + amazon_widget)
        soup = BeautifulSoup(r_amazon.text, "html.parser")
        amazon_link = soup.find("a", {"id": "titlehref"})
        if amazon_link:
            amazon_file.write(f"{index},{id},{amazon_link['href']}\n")


def export_amazon_links(filename: str):
    """Iterate the datasets/2020-08-19.csv, get the amazon url for each game (when possible) and saves it in the a file

    Args:
        filename: name of the file you want the data to be saved.
    """
    games = pd.read_csv("datasets/2020-08-19.csv")
    last_line_id = 0
    try:
        with open(filename, "r") as f:
            last_line = f.readlines()[-1]
        last_line_id = last_line.split(",")[0]
        print("The file already exsist, we will append missing amazon urls to it.")
    except:
        pass

    amazon_file = open(filename, "a")
    try:
        games[int(last_line_id) :].apply(
            lambda row: get_amazon_link(str(row.name), str(row.ID), amazon_file), axis=1
        )
    except:
        print(f"Unexpected error: {sys.exc_info()[0]}")
    amazon_file.close()


def concat_kaggel_amazon(amazon_file, output_csv_name):
    data = pd.read_csv(amazon_file, sep=",", header=None)
    data.columns = ["games_index", "ID", "amazon_link"]
    data["asin"] = data.apply(lambda row: row["amazon_link"].split("ASIN=")[1], axis=1)
    data = data.drop(columns="games_index")

    games = pd.read_csv("datasets/2020-08-19.csv")
    result = games.merge(data, on="ID", how="outer")

    result.to_csv(output_csv_name, index=False)

