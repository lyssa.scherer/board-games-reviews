# Laboratory - Data, Algorithms & Society - Computational Social Sciences & Humanities

### Topic 
Boardgame reviews

### Description
boardgamegeek.com is the globally most active community that includes extensive game reviews. In this lab, you will collect review data from from boardgamegeek and amazon and will identify and characterize similarities and differences in reviews between an expert community (bgg) and a more general audience (amazon).

### Team
**Supervisor:** Dr. Florian Lemmerich

**Team members:** Sourabh Zanwar and Lyssa Scherer
