import utils
import pandas as pd

# Get and save amazon urls into the file
"""utils.export_amazon_links(filename="datasets/amazon_links.txt")
utils.concat_kaggel_amazon("datasets/amazon_links.txt", "datasets/2020-08-19-extra.csv")"""

# Command used to get reviews from bbg, it took aprox. 3 days.
"""games_extra = pd.read_csv("datasets/2020-08-19-extra.csv")
games_extra_filled = games_extra[games_extra["amazon_link"].notna()]
game_ids = games_extra_filled["ID"].to_numpy()
utils.get_reviews(game_ids=game_ids, filename="datasets/reviews_bgg.csv")"""

# faster example
"""utils.get_reviews(game_ids=[30549], filename="datasets/reviews_example.csv")"""

