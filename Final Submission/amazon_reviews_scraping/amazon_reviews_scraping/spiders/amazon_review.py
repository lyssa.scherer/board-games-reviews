# -*- coding: utf-8 -*-
 
# Importing Scrapy Library
import scrapy
 
# Creating a new class to implement Spide
class AmazonReviewsSpider(scrapy.Spider):
 
    # Spider name
    name = 'amazon_reviews'
 
    # Domain names to scrape
    allowed_domains = ['amazon.com']
 
    # Base URL for the MacBook air reviews

 
    # Creating list of urls to be scraped by appending page number a the end of base url

    start_urls = []    
    with open('url_3.txt', 'r') as f:
        start_urls = f.readlines()
 
    # Defining a Scrapy parser
    def parse(self, response):
            data = response.css('#cm_cr-review_list')
            data2 = response.css('#cm_cr-product_info')
                                 
            #name of the product
            names = data2.css('.product-title')
 
            # Collecting product star ratings
            star_rating = data.css('.review-rating')
 
            # Collecting user reviews and information
            titles = data.css('.review-title')
            comments = data.css('.review-text')
            dates = data.css('.review-date')
            helpful = data.css('.cr-vote-text')
            count = 0
            
 
            # Combining the results
            for review in star_rating:
                yield{'name': ''.join(names.xpath('.//text()').extract()),
                      'review_title': ''.join(titles[count].xpath(".//text()").extract()),
		     'review_ID': ''.join(id[count].xpath(".//text()").extract()),
                      'rating': ''.join(review.xpath('.//text()').extract()),
                      'review': ''.join(comments[count].xpath(".//text()").extract()),
                      'date_posted': ''.join(dates[count].xpath(".//text()").extract()),
                      'helpful_votes': ''.join(helpful[count].xpath(".//text()").extract())
                     }
                count=count+1