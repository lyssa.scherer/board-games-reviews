#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This program will clean the review data, and remove the extra blank lines and tabs from the extracted reviews
@author: sourabhzanwar
"""

import pandas as pd
df0 = pd.read_csv('./spiders/amazon_reviews_temp.csv')
months = ['Jan', 'Feb', 'Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
df0 = df0.dropna()
df0 = df0.drop_duplicates()
df0 = df0[df0.name != 'name']
df0 = df0.reset_index()
df0 = df0.drop('index', axis =1 )

bl = []
for i in range(len(df0)):
    bl.append('Null')

df0['Country'] = bl
for i in range(len(df0)):
    #cleaning review title
    df0.review_title[i] = df0.review_title[i].strip()
    #cleaning review text
    text = df0.review[i]
    text = text.replace("\n", "")
    text = text.replace("          ", "")
    text = text.strip()
    df0.review[i] = text
    #getting correct date and country of review
    date = df0.date_posted[i]
    spl = date.split("on ")
    con = spl[0]
    date = spl[1]
    mon = 0
    for j in range(len(months)):
        if date[0:3] == months[j]:
            break
        else:
            mon = j+1
    date = date.replace(',','')
    date = date.split(" ")
    date = date[1] + '.' + str(mon) + '.' + date[2]
    df0.date_posted[i] = date
    con = con.split('in ')[1]
    if con[0:3] == 'the':
        con = con[4:]
    df0.Country[i] = con
    #cleaning ratings
    df0.rating[i] = int(df0.rating[i][0:1])
    #cleaning helpful votes
    if df0.helpful_votes[i][0:3] == 'One':
        df0.helpful_votes[i] = 1
    else:
        df0.helpful_votes[i] = int(df0.helpful_votes[i].split(' ')[0].replace(',',''))
del i,j,bl,con,date,mon,spl,text,months


df0.to_csv('./spiders/amazon_reviews.csv',index=False)