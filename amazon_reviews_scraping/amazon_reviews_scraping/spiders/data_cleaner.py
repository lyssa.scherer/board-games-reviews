#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This program will clean the review data, and remove the extra blank lines and tabs from the extracted reviews
@author: sourabhzanwar
"""

import pandas as pd
df0 = pd.read_csv("reviews.csv")

df0 = df0.dropna()
df0 = df0.reset_index()
df0 = df0.drop('index', axis =1 )
for i in range(len(df0)):
    text = df0.review[i]
    text = text.replace("\n", "")
    text = text.replace("          ", "")
    df0.review[i] = text
del text,i   

df0.to_csv("amazon_data.csv")