# -*- coding: utf-8 -*-
 
# Importing Scrapy Library
import scrapy
 
# Creating a new class to implement Spide
class AmazonReviewsSpider(scrapy.Spider):
 
    # Spider name
    name = 'amazon_reviews'
 
    # Domain names to scrape
    allowed_domains = ['amazon.com']
 
    # Base URL for the MacBook air reviews

 
    # Creating list of urls to be scraped by appending page number a the end of base url

    start_urls = []    
    with open('complete_url.txt', 'r') as f:
        start_urls = f.readlines()
 
    # Defining a Scrapy parser
    def parse(self, response):
            data = response.css('#cm_cr-review_list')
            data2 = response.css('#cm_cr-product_info')
                                 
            #name of the product
            names = data2.css('.product-title')
 
            # Collecting product star ratings
            star_rating = data.css('.review-rating')
 
            # Collecting user reviews
            comments = data.css('.review-text')
            count = 0
            
 
            # Combining the results
            for review in star_rating:
                yield{'name': ''.join(names.xpath('.//text()').extract()),
                      'stars': ''.join(review.xpath('.//text()').extract()),
                      'review': ''.join(comments[count].xpath(".//text()").extract())
                     }
                count=count+1