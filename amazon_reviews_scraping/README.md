<h2><ins>Install &#39;scrapy&#39; using the following command:</ins></h2>

<ul>
	<li>Use &quot;<strong>pip install scrapy</strong>&quot;&nbsp;if you use python</li>
	<li>Use &quot;<strong>conda install -c conda-forge scrapy</strong>&quot;&nbsp;to install it if you use anaconda platform</li>
</ul>

<h2><ins>Following is the procedure to run the crawler:</ins></h2>

<ol>
	<li>Install scrapy</li>
	<li>In a terminal open the folder where the repository is stored and change the directory to &#39;/amazon_reviews_scraping/amazon_reviews_scraping/spiders&#39;. You can use the following command from the repository folder &quot;<strong>cd /amazon_reviews_scraping/amazon_reviews_scraping/spiders</strong>&quot;</li>
	<li>To run the built spider that would scrape the reviews use the following command: &quot;<strong>scrapy runspider amazon_review.py -o reviews.csv</strong>&quot;</li>
	<li>Let the terminal open as the script runs, once it finishes, a file with name &#39;reviews.csv&#39; will be created.</li>
	<li>Now run the python program with the name &#39;data_cleaner.py&#39;, using the command &quot;<strong>python3 data_cleaner.py</strong>&quot;</li>
	<li>You now have the final file named &#39;amazon_data.csv&#39;</li>
</ol>
